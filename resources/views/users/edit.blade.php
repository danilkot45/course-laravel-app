@extends('layouts.admin')
@section('content')
    <div class="container pt-4 pb-4" style="background-color:white; border:2px solid white; border-radius:15px;">
        @if (isset($selected))
            <h1 class="text-warning">Редактирование пользователя:</h1>
            <hr class="text-warning">
            <form action="{!! url('/users', $user->id) !!}" method="POST">
                @method('PATCH')
            @else
                <h1>Создать нового пользователя</h1>
                <form action="{{ url('/users') }}" method="POST">
        @endif
        @csrf
        <div class="mb-3">
            <label for="name" class="form-label ">Имя</label>
            <input type="text" class="form-control @error('name') is-invalid @enderror" id="name" name="name"
                value="{{ old('name', $user->name ?? '') }}">
            @error('name')
                <div class="alert alert-danger">{{ $errors->first('name') }}</div>
            @enderror
        </div>

        <div class="mb-3">
            <label for="email" class="form-label ">Email</label>
            <input type="email" class="form-control @error('email') is-invalid @enderror" id="email" name="email"
                value="{{ old('email', $user->email ?? '') }}">

            @error('email')
                <div class="alert alert-danger">{{ $errors->first('email') }}</div>
            @enderror
        </div>

        <div class="mb-3">
            <label for="password" class="form-label ">Пароль</label>
            <input type="password" class="form-control @error('password') is-invalid @enderror" id="password"
                name="password">
            @error('password')
                <div class="alert alert-danger">{{ $errors->first('password') }}</div>
            @enderror
        </div>

        <div class="mb-3">
            <label for="confirm_password" class="form-label ">Повторите пароль</label>
            <input type="password" class="form-control @error('confirm_password') is-invalid @enderror"
                id="confirm_password" name="confirm_password">
            @error('confirm_password')
                <div class="alert alert-danger">{{ $errors->first('confirm_password') }}</div>
            @enderror
        </div>
        <button type="submit" class="btn btn-info">Сохранить</button>
        </form>
    </div>
@endsection
