@extends('layouts.admin')
@section('content')
    <div class="container" style="padding:0;">
        <div class="d-flex row">
            @if (isset($nameTag))
                <h1 class="row justify-content-center " style="width:100%;font-weight:bold; margin:0 auto;">Статьи по тегу:
                    {{ $nameTag }}</h1>
                <h3 class="row justify-content-center" style="width:100%;margin:0 auto;">Кто ищет, тот всегда найдет!</h3>
            @else
                <h1 class="row justify-content-center" style="width:100%;font-weight:bold; margin:0 auto;">Множество
                    интересных
                    статей</h1>
                <h3 class="row justify-content-center" style="width:100%;margin:0 auto;">Напиши свою интересную статью!
                </h3>
                <a href="{{ url('/articles/create') }}" class='btn btn-info  mt-3  font-bold'
                    style=" width:100%;border: 2px solid white; border-radius:15px; font-weight:bold; width:400px; margin:0 auto; ">Написать статью</a>
            @endif
        </div>
    </div>
    <div class="row align-item-center justify-content-center mt-3" style="width:100%;">
        @foreach ($articles as $article)
            <div class="col-md-3 mb-3 me-2 mr-1 ml-1 pb-4" style="border:4px solid white; border-radius:15px;">
                <article>
                    <h2>
                        <a href="{{ url('/articles', $article->id) }}"
                            style="text-decoration:none; ">{{ $article->title }}</a>
                    </h2>
                    <hr class="text-black">
                    <div class="time " style=" display:flex; ">{{ $article->published_at }}</div>
                </article>
            </div>
        @endforeach
        <nav class="d-flex row col-12 align-item-center justify-content-center">
            <ul class="pagination justify-content-center">
                <li>{{ $articles->links() }}</li>
            </ul>
        </nav>
    </div>
    </div>
@endsection
