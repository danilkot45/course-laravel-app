@extends('layouts.admin')

@section('content')

    <div class="container">
        <div class="row d-flex">
            <div class="row ">
                <h1 class="mb-3 col-12" style="font-weight:bold">{{ $article->title }}</h1>
                <h4 class="author col-12">Статью опубликовал: {{ $author }}</h4>
                <h6 class="time col-12">Опубликовано {{ $wherePublished }}</h6>
                @if ($article->tags->isNotEmpty())
                    <div class="tags d-flex align-items-center col-12">
                        <h5 style="margin-top:7px;font-weight:bold;">Теги: </h5>
                        @foreach ($article->tags as $tag)
                            <a href="{{ url('/tags', $tag->name) }}"class="btn btn-info mx-2 ">{{ $tag->name }}</a>
                        @endforeach
                    </div>
                @endif
                <a href="{{ url('articles/'.$article->id.'/edit') }}" class="mt-3 btn btn-warning ">Редактировать</a>
                <p class="mt-5 col-12" style="padding:15px;font-weight:bold;background-color:white;line-height:32px;border: 2px solid #007bff;
                border-radius: 5px;">{{ $article->body }}</p>
            </div>
        </div>
    </div>
@endsection
