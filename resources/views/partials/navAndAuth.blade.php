<nav class="navbar navbar-expand-md navbar-light  shadow-sm w-100">
    <div class="container">
        <a class="navbar-brand nav-link " href="{{ url('/articles') }}">
            {{ config('app.name', 'Laravel') }}
        </a>
        <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent"
            aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="{{ __('Toggle navigation') }}">
            <span class="navbar-toggler-icon"></span>
        </button>

        <div class="collapse navbar-collapse" id="navbarSupportedContent">
            <!-- Left Side Of Navbar -->
            <ul class="navbar-nav me-auto nav">
                <li class="nav-item">
                    <a class="nav-link " href="/articles">Все статьи</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="/articles/create">Написать статью</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="{{ url('/articles', $latest->id) }}">Последняя актуальная
                        статья: {{ $latest->title }}</a>
                </li>
            </ul>
        </div>
    </div>
</nav>
