<?php

namespace App\Http\Controllers;

use DataTables;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use App\Http\Requests\PublishOrUpdateUserRequest;

class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function index(Request $request)
    {
        if ($request->ajax()) {
            $data = User::select('id', 'name', 'email')->get();
            return Datatables::of($data)->addIndexColumn()
                ->addColumn('action', function ($row) {
                    return '<a href="users/' . $row->id . '" class="btn btn-success btn-sm">View</a>
                    <a href="users/' . $row->id . '/edit" class="btn btn-warning btn-sm">Edit</a>';
                })
                ->rawColumns(['action'])
                ->make(true);
        }

        return view('users.index');
    }

    public function create(User $user)
    {
        return view('users.edit')->with([
            'user' => $user,
        ]);
    }
    public function store(PublishOrUpdateUserRequest $request)
    {
        $userclone = $request->validated();
        $userclone['password'] =  Hash::make($request->password);
        User::create($userclone);
        return redirect('users');
    }
    public function edit($user)
    {
        $selected = true;
        $user = User::find($user);
        return view('users/edit')->with([
            'user' => $user,
            'selected' => $selected
        ]);
    }
}
