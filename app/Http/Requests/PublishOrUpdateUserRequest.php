<?php

namespace App\Http\Requests;

use Illuminate\Validation\Rules\Password;
use Illuminate\Foundation\Http\FormRequest;

class PublishOrUpdateUserRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name'  => 'required|min:2|max:15',
            'email' => 'required|email|unique:users,email',
            'password'  => ['required', Password::min(8)->mixedCase()->uncompromised(3)],
            'confirm_password' => 'required|same:password'
        ];
    }
}
